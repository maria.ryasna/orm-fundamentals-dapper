﻿using Dapper.Store.Core.Entities;
using System.Threading.Tasks;

namespace Dapper.Store.Core.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task CreateAsync(TEntity value);
        Task<TEntity> ReadAsync(int id);
        Task UpdateAsync(TEntity value);
        Task DeleteAsync(int id);
    }

}
