﻿using Dapper.Store.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dapper.Store.Core.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        Task<IEnumerable<Order>> GetAllOrdersByFilterAsync(OrderFilter filter);
        Task BulkDeleteOrdersAsync(OrderFilter filter);

    }
}
