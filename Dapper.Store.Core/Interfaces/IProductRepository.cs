﻿using Dapper.Store.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Dapper.Store.Core.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        Task<IEnumerable<Product>> GetAllProductsAsync();
    }
}
