﻿using Dapper.Store.Core.Enums;
using System;

namespace Dapper.Store.Core.Entities
{
    public class Order : BaseEntity
    {
        public OrderStatus Status { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime UpdatedDate { get; set; }
        public int ProductId { get; set; }
    }
}
