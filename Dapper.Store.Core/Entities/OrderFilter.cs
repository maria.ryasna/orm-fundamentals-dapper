﻿using Dapper.Store.Core.Enums;

namespace Dapper.Store.Core.Entities
{
    public class OrderFilter
    {
        public int? Month = null;
        public OrderStatus? Status = null;
        public int? Year = null;
        public int? ProductId = null;
    }
}
