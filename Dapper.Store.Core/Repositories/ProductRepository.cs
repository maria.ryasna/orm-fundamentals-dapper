﻿using Dapper.Store.Core.Entities;
using Dapper.Store.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Dapper.Store.Core.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly string _connectionString;
        public ProductRepository(string connectionString)
        {
            _connectionString = connectionString;
        }
        public async Task CreateAsync(Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            await using var connection = new SqlConnection(_connectionString);
            string query = $"SET IDENTITY_INSERT [dbo].[Product] ON; INSERT INTO[dbo].[Product] ([Id],[Name],[Description],[Weight],[Height],[Width],[Length])  VALUES(@Id,@Name,@Description,@Weight,@Height,@Width,@Length); SET IDENTITY_INSERT [dbo].[Product] OFF;";
            await connection.ExecuteAsync(query, product);
        }

        public async Task DeleteAsync(int id)
        {
            await using var connection = new SqlConnection(_connectionString);
            string query = $"DELETE FROM [dbo].[Product] WHERE Id = @Id";
            await connection.ExecuteAsync(query, id);
        }

        public async Task<IEnumerable<Product>> GetAllProductsAsync()
        {
            await using var connection = new SqlConnection(_connectionString);
            string query = $"SELECT * FROM [dbo].[Product]";
            var products = await connection.QueryAsync<Product>(query);
            return products;
        }

        public async Task<Product> ReadAsync(int id)
        {
            await using var connection = new SqlConnection(_connectionString);
            string query = $"SELECT * FROM [dbo].[Product] WHERE Id = @Id";
            var product = await connection.QueryAsync<Product>(query, new Product { Id = id });
            return product.FirstOrDefault();
        }

        public async Task UpdateAsync(Product product)
        {
            if (product is null)
            {
                throw new ArgumentNullException(nameof(product));
            }
            await using var connection = new SqlConnection(_connectionString);
            var query = @$"UPDATE [dbo].[Product] SET 
                        Name = @Name, 
                        Description = @Description,
                        Weight = @Weight, 
                        Height = @Height, 
                        Width = @Width, 
                        Length = @Length
                        WHERE Id = @Id";
            await connection.ExecuteAsync(query, product);
        }
    }
}
