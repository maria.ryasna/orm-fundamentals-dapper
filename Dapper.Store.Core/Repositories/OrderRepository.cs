﻿using Dapper.Store.Core.Entities;
using Dapper.Store.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Dapper.Store.Core.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly string _connectionString;
        private readonly string dateTimeFormat = "MM/dd/yyyy HH:mm:ss";
        public OrderRepository(string connectionString)
        {
            _connectionString = connectionString;
        }

        public async Task BulkDeleteOrdersAsync(OrderFilter filter)
        {
            await using var connection = new SqlConnection(_connectionString);
            await connection.OpenAsync();
            using (var transaction = connection.BeginTransaction())
            {
                string query = $"spOrder_BulkDeleteOrders";
                await connection.QueryAsync<Order>(query, filter, commandType: CommandType.StoredProcedure, transaction: transaction);
                transaction.Commit();
            }
        }

        public async Task CreateAsync(Order order)
        {
            if (order is null)
            {
                throw new ArgumentNullException(nameof(order));
            }
            await using var connection = new SqlConnection(_connectionString);
            string query = $"SET IDENTITY_INSERT [dbo].[Order] ON; INSERT INTO [dbo].[Order] ([Id],[Status],[CreatedDate],[UpdatedDate],[ProductId]) VALUES (@Id,@Status,'{DateTime.Now.ToString(dateTimeFormat)}','{DateTime.Now.ToString(dateTimeFormat)}',@ProductId); SET IDENTITY_INSERT [dbo].[Order] OFF;";
            await connection.ExecuteAsync(query, order);
        }

        public async Task DeleteAsync(int id)
        {
            await using var connection = new SqlConnection(_connectionString);
            string query = $"DELETE FROM [dbo].[Order] WHERE Id = @Id";
            await connection.ExecuteAsync(query, id);
        }

        public async Task<IEnumerable<Order>> GetAllOrdersByFilterAsync(OrderFilter filter)
        {
            await using var connection = new SqlConnection(_connectionString);
            string query = $"spOrder_GetAllOrdersByFilter";
            var result = await connection.QueryAsync<Order>(query, filter, commandType: CommandType.StoredProcedure);
            return result;
        }

        public async Task<Order> ReadAsync(int id)
        {
            await using var connection = new SqlConnection(_connectionString);
            string query = $"SELECT * FROM [dbo].[Order] WHERE Id = @Id";
            var order = await connection.QueryAsync<Order>(query, new Product { Id = id });
            return order.FirstOrDefault();
        }

        public async Task UpdateAsync(Order order)
        {
            await using var connection = new SqlConnection(_connectionString);
            var query = @$"UPDATE [dbo].[Order] SET 
                        Status = @Status, 
                        UpdatedDate = '{DateTime.Now.ToString(dateTimeFormat)}',
                        ProductId = @ProductId
                        WHERE Id = @Id";
            await connection.ExecuteAsync(query, order);
        }
    }
}
