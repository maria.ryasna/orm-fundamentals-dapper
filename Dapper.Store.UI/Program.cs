﻿using Dapper.Store.Core;
using Dapper.Store.Core.Entities;
using Dapper.Store.Core.Enums;
using Dapper.Store.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;

namespace Dapper.Store.UI
{
    internal class Program
    {
        static void Main(string[] args)
        {
            MainAsync().Wait();
            Console.ReadKey();
        }
        static async Task MainAsync()
        {
            ConnectionStringSettingsCollection settings = ConfigurationManager.ConnectionStrings;

            if (settings != null)
            {
                foreach (ConnectionStringSettings param in settings)
                {
                    IUnitOfWork uow = new UnitOfWork(param.ConnectionString);

                    IEnumerable<Product> products = await uow.ProductRepository.GetAllProductsAsync();

                    foreach (Product product in products)
                    {
                        ShowProduct(product);
                    }

                    await uow.OrderRepository.BulkDeleteOrdersAsync(new OrderFilter { Month = 5 });

                    IEnumerable<Order> orders = await uow.OrderRepository.GetAllOrdersByFilterAsync(new OrderFilter { Month = 3 });

                    Console.WriteLine("Orders:");
                    foreach (Order order in orders)
                    {
                        ShowOrder(order);
                    }

                    await uow.OrderRepository.UpdateAsync(new Order { Id = 6, Status = OrderStatus.Done, ProductId = 1 });

                    ShowProduct(await uow.ProductRepository.ReadAsync(2));
                }
            }
        }

        public static void ShowOrder(Order order)
        {
            Console.WriteLine($"Id: {order.Id}");
            Console.WriteLine($"Order status: {order.Status}");
            Console.WriteLine($"Created: {order.CreatedDate}");
            Console.WriteLine($"Updated: {order.UpdatedDate}");
        }

        public static void ShowProduct(Product product)
        {
            Console.WriteLine($"Id: {product.Id}");
            Console.WriteLine($"Product name: {product.Name}");
            Console.WriteLine($"Description: {product.Description}");
            Console.WriteLine("Size: " + product.Width + "x" + product.Height + "x" + product.Length);
            Console.WriteLine($"Product name: {product.Weight}");
        }
    }
}
